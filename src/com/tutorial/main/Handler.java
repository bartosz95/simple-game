package com.tutorial.main;

import java.awt.*;
import java.util.LinkedList;

public class Handler {
    LinkedList<GameObject> objects = new LinkedList<GameObject>();

    public int spd = 5;

    public void tick(){
        for(int i=0; i<objects.size(); i++){
            GameObject tempObject = objects.get(i);
            tempObject.tick();
        }
    }

    public void render(Graphics g){
        for(int i =0; i < objects.size(); i++){
            GameObject tempObject = objects.get(i);
            tempObject.render(g);
        }
    }

    public void clearEnemys(){
        GameObject player = null;
        while (objects.size()>0){
               if(objects.getFirst().getId()==ID.Player)
                   player=objects.getFirst();
               objects.removeFirst();
        }
        if(player != null && (Game.gameState != Game.STATE.End))
            objects.add(player);
    }

    public void addObject(GameObject object){
        this.objects.add(object);
    }

    public void removeObject(GameObject object){
        this.objects.remove(object);
    }

    public int size(){
        return objects.size();
    }
}
