package com.tutorial.main;

import java.awt.*;
import java.util.Random;

public class BossEnemy extends GameObject {

    private Handler handler;
    private Color color;
    private int size;
    Random r = new Random();

    private int timer = 80;
    private int timer2 = 50;

    public BossEnemy(float x, float y, ID id, Handler handler){
        super(x, y, id);

        this.handler = handler;
        this.color = Color.red;
        this.size = 90;
        velX = 0;
        velY = 2;
    }
    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
    }

    public void tick(){
        x += velX;
        y += velY;
        if(timer <=  0) velY = 0;
        else timer --;

        if(timer <= 0) timer2--;
        if(timer2 <= 0){
            if(velX == 0) velX = 2;
            if(velX > 0) velX += 0.005f;
            if(velX < 0) velX -= 0.005f;

            velX = Game.clamp(velX,-10,10);
            int spawn = r.nextInt(10);
            if(spawn == 0)  handler.addObject(new BossEnemyBullet((int)x+size/2,(int)y+size/2, ID.BasicEnemy, handler));
        }
        //if(y <= 0 || y >= Game.HEIGHT -129) velY *= -1;
        if(x <= 0 || x >= Game.WIDTH -106) velX *= -1;
        //x = Game.clamp((int)x, 0, Game.WIDTH-106);
        //y = Game.clamp((int)y, 0, Game.HEIGHT-129);

        //handler.addObject(new Trail((int)x, (int)y,ID.Trail, color, size, size, 0.008f, handler));
    }

    public void render(Graphics g){
        g.setColor(color);
        g.fillRect((int)x, (int)y, size, size);
    }
}
