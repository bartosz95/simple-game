package com.tutorial.main;

import java.awt.*;

public class HUD {

    public int bounds = 0;
    public static float HEALTH = 100f;
    private float greenValue = 255f;

    private int score = 0;
    private int level = 0;


    public void tick(){
        HEALTH = Game.clamp(HEALTH, 0f ,100f + bounds/2);
        greenValue = HEALTH*2;
        greenValue = Game.clamp(greenValue, 0f, 255f);
        score++;
    }

    public void render(Graphics g){
        g.setColor(Color.gray);
        g.fillRect(15,15, 200 + bounds, 32);
        g.setColor(new Color(75, (int)greenValue, 0));
        g.fillRect(15,15, (int)HEALTH*2, 32);
        g.setColor(Color.white);
        g.drawRect(15,15, 200 + bounds, 32);

        g.drawString("Score: " + score, 15, 64);
        g.drawString("Level: " + level, 15, 80);
        g.drawString("Space for Shop: ", 15, 96);
    }

    public void score(int score){
        this.score = score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public  int getScore(){
        return score;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public int getLevel(){
        return level;
    }
}
