package com.tutorial.main;

import java.awt.*;
import java.util.Random;

public class HardEnemy extends GameObject {

    private Handler handler;
    private Color color;
    private int size;
    private Random r = new Random();

    public HardEnemy(float x, float y, ID id, Handler handler){
        super(x, y, id);

        this.handler = handler;
        this.color = Color.yellow;
        this.size = 16;
        velX = 3;
        velY = 3;
    }
    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
    }

    public void tick() {
        x += velX;
        y += velY;

        if (y <= 0 || y >= Game.HEIGHT - 55) {
            if (velY < 0) {
                velY = -(r.nextInt(7) + 1) * -1;
            } else {
                velY = (r.nextInt(7) + 1) * -1;
            }
        }
        if (x <= 0 || x >= Game.WIDTH - 32) {
            if (velX < 0) {
                velX = -(r.nextInt(7) + 1) * -1;
            } else {
                velX = (r.nextInt(7) + 1) * -1;
            }
        }
        handler.addObject(new Trail((int)x, (int)y,ID.Trail, color, size, size, 0.02f, handler));
    }

    public void render(Graphics g){
        g.setColor(color);
        g.fillRect((int)x, (int)y,size,size);
    }
}
