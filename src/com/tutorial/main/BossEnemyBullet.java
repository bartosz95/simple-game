package com.tutorial.main;

import java.awt.*;
import java.util.Random;

public class BossEnemyBullet extends GameObject {

    private Handler handler;
    private Color color;
    private int size;
    Random r = new Random();

    public BossEnemyBullet(float x, float y, ID id, Handler handler){
        super(x, y, id);
        this.handler = handler;
        this.color = Color.red;
        this.size = 10;

        velX = (r.nextInt(5 - -5) + -5);
        velY = 5;
    }
    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
    }

    public void tick(){
        x += velX;
        y += velY;

        if(y >= Game.HEIGHT) handler.removeObject(this);

        handler.addObject(new Trail((int)x, (int)y,ID.Trail, color, size, size, 0.02f, handler));
    }

    public void render(Graphics g){
        g.setColor(color);
        g.fillRect((int)x, (int)y,size,size);
    }
}
