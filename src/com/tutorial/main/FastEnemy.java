package com.tutorial.main;

import java.awt.*;

public class FastEnemy extends GameObject {

    private Handler handler;
    private Color color;
    private int size;

    public FastEnemy(float x, float y, ID id, Handler handler){
        super(x, y, id);
        this.handler = handler;
        this.color = Color.cyan;
        this.size = 16;
        velX = 3;
        velY = 9;
    }
    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
    }

    public void tick(){
        x += velX;
        y += velY;

        if(y <= 0 || y >= Game.HEIGHT -55) velY *= -1;
        if(x <= 0 || x >= Game.WIDTH - 32) velX *= -1;

        handler.addObject(new Trail((int)x, (int)y, ID.Trail, color, size, size, 0.02f, handler));
    }

    public void render(Graphics g){
        g.setColor(color);
        g.fillRect((int)x, (int)y, size, size);
    }
}

