package com.tutorial.main;

public enum ID {

    Player(),
    Trail,
    BasicEnemy(),
    FastEnemy,
    SmartEnemy,
    BossEnemy,
    HardEnemy,
    MenuParticle,
}
