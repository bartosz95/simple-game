package com.tutorial.main;

import java.awt.*;

public class SmartEnemy extends GameObject {

    private Handler handler;
    private Color color;
    private int size;
    private GameObject player;

    public SmartEnemy(float x, float y, ID id, Handler handler){
        super(x, y, id);

        this.handler = handler;
        this.color = Color.green;
        this.size = 16;

        for(int i=0; i < handler.objects.size(); i++){
            if(handler.objects.get(i).getId() == ID.Player) player = handler.objects.get(i);
        }

    }
    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
    }

    public void tick(){
        x += velX;
        y += velY;

        float diffX = x - player.getX() - size;
        float diffY = y - player.getY() - size;
        float distance = (float) Math.sqrt( (x-player.getX()) * (x-player.getX()) + (y-player.getY()) * (y-player.getY()));


        velX = ((-2/distance) * diffX);
        velY = ((-2/distance) * diffY);

        if(y <= 0 || y >= Game.HEIGHT -55) velY *= -1;
        if(x <= 0 || x >= Game.WIDTH -32) velX *= -1;

        handler.addObject(new Trail((int)x, (int)y,ID.Trail, color, size, size, 0.02f, handler));
    }

    public void render(Graphics g){
        g.setColor(color);
        g.fillRect((int)x, (int)y,size,size);
    }
}

