package com.tutorial.main;

import java.awt.*;
import java.util.Random;

public class Player extends GameObject {

    Random r = new Random();
    Handler handler;
    private int size;

    public Player(int x, int y, ID id, Handler handler){
        super(x,y,id);
        this.handler = handler;
        this.size = 32;
    }

    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
        }

    public void tick(){
        x += velX;
        y += velY;

        x = Game.clamp((int)x, 0, Game.WIDTH-48);
        y = Game.clamp((int)y, 0, Game.HEIGHT-71);


        //handler.addObject(new Trail((int)x, (int)y,ID.Trail, Color.white, size, size, 0.05f, handler));

        collision();
    }

    private void collision(){
        for(int i =0; i < handler.objects.size(); i++){

            GameObject tempObject = handler.objects.get(i);

            if(tempObject.getId() == ID.BasicEnemy || tempObject.getId() == ID.BasicEnemy || tempObject.getId() == ID.SmartEnemy){
                if(getBounds().intersects(tempObject.getBounds())){
                    HUD.HEALTH -= 2;
                }
            }
        }
    }

    public void render(Graphics g){
        g.setColor(Color.white);
        g.fillRect((int)x, (int)y, size, size);
    }
}
