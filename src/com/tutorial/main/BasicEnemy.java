package com.tutorial.main;

import java.awt.*;

public class BasicEnemy extends GameObject {

    private Handler handler;
    private Color color;
    private int size;

    public BasicEnemy(float x, float y, ID id, Handler handler){
        super(x, y, id);

        this.handler = handler;
        this.color = Color.red;
        this.size = 16;
        velX = 3;
        velY = 3;
    }
    public Rectangle getBounds(){
        return new Rectangle((int)x, (int)y,size,size);
    }

    public void tick(){
        x += velX;
        y += velY;

        if(y <= 0 || y >= Game.HEIGHT -55) velY *= -1;
        if(x <= 0 || x >= Game.WIDTH -32) velX *= -1;

        handler.addObject(new Trail((int)x, (int)y,ID.Trail, color, size, size, 0.02f, handler));
    }

    public void render(Graphics g){
        g.setColor(color);
        g.fillRect((int)x, (int)y,size,size);
    }
}
